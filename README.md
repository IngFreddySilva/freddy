# Notas académica

### Pre-requisitos 📋

- PHP >= 7.2 
- Composer versión 2. 
- node >= 14.15
- npm
- yarn

### Instalación 🔧

1. Clonar o descargar y descomprimir el codigo

2. Instalar paquetes de composer ejecutando `composer install`.

3. Instalar paquetes de node `npm install && npm run prod`

4. Configurar VirtualHost (httpd-vhosts.conf):     
    <VirtualHost *:80>
        DocumentRoot "D:/htdocs/freddy/public"
        ServerName freddy.eva
        <Directory "D:/htdocs/freddy/public">
            DirectoryIndex index.php
            Options Indexes FollowSymLinks
            AllowOverride All
            Require all granted
        </Directory>
    </VirtualHost>

5. Configurar el archivo hosts (C:\Windows\System32\drivers\etc\hosts)
        127.0.0.1 freddy.eva

6. Acceder al sitio por la url `http://freddy.com.eva/inicio`

------------------------