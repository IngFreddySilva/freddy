<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


class HomeController extends AbstractController
{
    public function index(): Response
    {
        return $this->render('home/index.html.twig');
    }

    public function eventosUsuarios(Request $request)
    {        
        $data = file_get_contents($this->getParameter('datos_dir').'MOCK_DATA.json');
        $data = $this->json(json_decode($data, true));

        return $data;
    }
}